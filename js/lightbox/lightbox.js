import gallery from "../gallery/gallery.js"
import loader from '../photoloader/photoloader.js';

let imgCourante;
let titre;
let htmlimg;
let uri;

let init = function (data){
    imgCourante = data.img;
    titre = data.titre;
    htmlimg = data.htmlimg;
    uri = data.uri;
};

let showLightBox = function(){
    $("#lightbox_title").text(titre);
    $("#lightbox_full_img").attr("src",imgCourante);
    $("#lightbox_container").show();
    infosPhoto(uri);
    commentairesLightBox(uri);
};

let closeLightBox = function(){
    $("#lightbox_container").hide();
};

let infosPhoto = function(param) {
    let promise = loader.loaderInfosPhotos(param);
    promise.then(infosPhotoLightBox);
};

let commentairesLightBox = function(param) {
    let promise = loader.loaderInfosPhotos(param + "/comments");
    promise.then(afficherComments);
};

let afficherComments = function(response){
   $(".com").remove();
    response.data.comments.forEach((elem)=>{
        $("#commentaires").append(`<div class="com"> 
                                    <p>id: ${elem.id} </p>
                                    <p>Titre: ${elem.titre}</p>
                                    <p>Pseudo: ${elem.pseudo}</p>
                                    <p>Contenu: ${elem.content}</p>
                                    <p>Date: ${elem.date}</p>
                                    </div>`);
    })
};
let  ajouterCom = function(param){
    return axios.post(param + uri + "/comments",null,{
        withCredentials:true,
        responseType: 'json',
        data:{
            titre: $("#titre").val(),
            pseudo: $("#pseudo").val(),
            content: $("#contenu").val()
        }
    }).catch((e)=>{
        alert("Erreur dans l'envoie de votre commentaire !");
    }).then(() =>{
        alert("Votre commentaire à bien été envoyé !");
    });
};

let infosPhotoLightBox = function(reponse){
    let infos = reponse.data.photo;
    $("#id").text("Id: " + infos.id);
    $("#descr").text("Description: " + infos.descr);
    $("#format").text("Format: " + infos.format);
    $("#type").text("Type: " + infos.type);
    $("#size").text("Taille: " + infos.size);
    $("#width").text("Largeur: " + infos.width);
    $("#height").text("Hauteur: " + infos.height);
};

let showLightBoxSuiv = function(){
    imgCourante = htmlimg.next().children("img");
    uri = htmlimg.next().children("img").data("uri");
    if (imgCourante.length == 0){
        let firstChild = $('.gallery-container').children().first();
        imgCourante = firstChild.children("img");
        uri = firstChild.children("img").data("uri");
    }
    infosPhoto(uri);
    commentairesLightBox(uri);
    $("#lightbox_full_img").attr("src",imgCourante.data("img"));
    htmlimg = imgCourante.parent();
    $("#lightbox_title").text(htmlimg.children().last()[0].innerHTML);
};

let showLightBoxPrev = function(){
    imgCourante = htmlimg.prev().children("img");
    uri = htmlimg.prev().children("img").data("uri");
    if (imgCourante.length == 0){
      let lastChild = $('.gallery-container').children().last();
      imgCourante = lastChild.children("img");
      uri = lastChild.children("img").data("uri");
    }
    infosPhoto(uri);
    commentairesLightBox(uri);
    $("#lightbox_full_img").attr("src",imgCourante.data("img"));
    htmlimg = imgCourante.parent();
   $("#lightbox_title").text(htmlimg.children().last()[0].innerHTML);

};

export default{
    showLightBox:showLightBox,
    showLightBoxPrev:showLightBoxPrev,
    showLightBoxSuiv:showLightBoxSuiv,
    closeLightBox:closeLightBox,
    ajouterCom:ajouterCom,
    init:init
}