
let server_url;
let server_path;

let init = function (config) {
    server_url = config.url;
    server_path = config.path;
};

let loaderObjectList = function(param){
    return axios.get(server_url + server_path + param, {
        responseType: 'json',
        withCredentials:true,
    }).catch((e)=>console.error(e));
};
let loaderInfosPhotos = function(param){
    return axios.get(server_url + param, {
        responseType: 'json',
        withCredentials:true,
    }).catch((e)=>console.error(e));
};

let getServUrl = function () {
    return server_url;
};

export default {
    init:init,
    loaderObjectList:loaderObjectList,
    getServUrl:getServUrl,
    loaderInfosPhotos:loaderInfosPhotos,
}
