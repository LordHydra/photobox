import loader from '../photoloader/photoloader.js';
import lightbox from '../lightbox/lightbox.js';

let idgalerie;
let offset;
let size;

let init = function (config) {
    idgalerie = config.idgalerie;
    loader.init(config);
    offset = config.offset;
    size = config.size;

};

let  buildGallery = function (response) {
    let gallery = response.data.photos.reduce( (gall ,ph) =>
        gall.append($(`<div class="vignette">
                        <img data-img="${loader.getServUrl()+ph.photo.original.href}"
                             data-uri="${ph.links.self.href}"
                             src="${loader.getServUrl()+ph.photo.thumbnail.href}">
                        <div>${ph.photo.titre}</div>`))
        ,$('<div class="gallery-container" id="gallery_container"></div>'));

  let gallery_contain = $(idgalerie);
  gallery_contain.find('#gallery_container').remove();
  gallery_contain.prepend(gallery);
  brancherHandlers();
};

let brancherHandlers = function (){
    $('.vignette > img').on("click", function (e) {
        let data = {
            htmlimg : $(e.target).parent(),
            img : $(e.target).data("img"),
            titre : $(e.target).next()[0].innerHTML,
            off : offset,
            taille : size,
            uri : $(e.target).data("uri"),
        };
        lightbox.init(data);
        lightbox.showLightBox();
    });
};

let showGallery = function(){
    let promise = loader.loaderObjectList(`?offset=${offset}&size=${size}`);
    promise.then(buildGallery)
};

let showGallerysuiv = function(){
    let promise = loader.loaderObjectList(`?offset=${offset+10}&size=${size}`);
    offset+= 10;
    promise.then(buildGallery)
};

let showGalleryprec = function(){
    let promise = loader.loaderObjectList(`?offset=${offset-10}&size=${size}`);
    offset -= 10;
    promise.then(buildGallery)
};

export default {
    init:init,
    showGall:showGallery,
    showGallerysuiv:showGallerysuiv,
    showGalleryprec:showGalleryprec,
}

