import gallery from "./gallery/gallery.js"
import lightbox from "./lightbox/lightbox.js";

const SERV_URL = "https://webetu.iutnc.univ-lorraine.fr";
const PATH_GAL = "/www/canals5/photobox/photos/";

$(document).ready( ()=> {
   let conf = {
      idgalerie : "#gallery",
      url : SERV_URL,
      path : PATH_GAL,
      offset : 0,
      size : 10,
   };
   gallery.init(conf);

   let buttonLoad = $("#load_gallery");
   buttonLoad.on("click", function () {
      gallery.showGall();
   });

   let buttonsuiv = $("#next");
   buttonsuiv.on("click", function () {
      gallery.showGallerysuiv();
   });

   let buttonprec = $("#previous");
   buttonprec.on("click", function () {
      gallery.showGalleryprec();
   });

   let buttonCloseLightBox =  $("#lightbox_close");
      buttonCloseLightBox.on("click", function (e) {
         lightbox.closeLightBox();
       });

   let buttonprecLightBox = $("#lightbox_prec");
  buttonprecLightBox.on("click", function (e) {
      lightbox.showLightBoxPrev();
   });

   let buttonsuivLightBox =  $("#lightbox_suiv");
   buttonsuivLightBox.on("click", function (e) {
      lightbox.showLightBoxSuiv();
   });

    $("#boutonValider").on("click", function(e){
        lightbox.ajouterCom(SERV_URL);
    });
});